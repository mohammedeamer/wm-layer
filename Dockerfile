FROM nvidia/cuda:10.2-cudnn7-runtime-ubuntu18.04

MAINTAINER Mohammed E. Amer <mohammed.e.amer@gmail.com>

RUN apt update

RUN apt install -y python3-pip

RUN pip3 install --upgrade pip

COPY requirements.txt .

RUN pip3 install -r requirements.txt

WORKDIR /usr/src/results/

ENTRYPOINT ["./run.sh"]
