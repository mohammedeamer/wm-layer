#!/usr/bin/python3

'''
    Usage: ./start.py <script_path> <script_args>
    Note: script_path and any provided paths should be relative to results
    folder
'''

import sys
import os
import subprocess
import logging

this_path = os.path.dirname(os.path.realpath(__file__))

data_path = os.path.join(this_path, 'data')
raw_path = os.path.join(this_path, 'raw')
results_path = os.path.join(this_path, 'results')

image_name = 'datascience'


def run(args):

    docker_run = ['docker', 'run', '-it',
                  '--network', 'host', '-e', 'LANG=C.UTF-8']

    try:

        process = subprocess.Popen(['nvidia-smi'], stdout=subprocess.PIPE)
        process.communicate()

        rc = process.returncode

    except Exception:

        rc = -1

    if rc == 0:

        docker_run.append('--runtime=nvidia')
        logging.info('Nvidia detected')

    else:
        logging.info('No Nvidia detected')

    docker_run += ['-v', f'{data_path}:/usr/src/data',
                   '-v', f'{raw_path}:/usr/src/raw',
                   '-v', f'{results_path}:/usr/src/results',
                   image_name]

    docker_run += args

    docker_build = ['docker', 'build', '-t', image_name, this_path]

    subprocess.run(docker_build)
    subprocess.run(docker_run)


if __name__ == '__main__':

    args = sys.argv[1:]

    run(args)
