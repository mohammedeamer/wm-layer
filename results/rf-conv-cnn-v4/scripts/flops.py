#!/usr/bin/python3

import argparse, importlib.util, json

import numpy as np

RELU_FLOPS = 1.0

def load_config(f, t):

	if t == 'python':

		spec = importlib.util.spec_from_file_location("config", f)
		config = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(config)

		return config.CONFIG

	if t == 'json':
		with open(f, 'r') as text:
			return json.load(text)

def calc_flops(config, i):

	conv_layers = config['conv_layers']

	flops = 0.0

	for layer in conv_layers:

		k = layer['field']
		stride = layer['step']
		in_chs = layer['in_chs']
		out_chs = layer['out_chs']
		padding = layer['padding']

		# pre-kernel mul
		flops += i*i

		out_dim = np.floor(((i + 2*padding - (k - 1) - 1)/stride) + 1)

		i = out_dim

		# mul
		flops += out_dim*out_dim*k*k*in_chs*out_chs

		# add
		flops += out_dim*out_dim*(k*k*in_chs -1)*out_chs

		# bias
		flops += out_dim*out_dim*out_chs

		# ReLU
		flops += out_dim*out_dim*out_chs*RELU_FLOPS

	return flops

def run(t, f, i):

	config = load_config(f, t)

	print(calc_flops(config, int(i)))

if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	parser.add_argument("-t", action="store")
	parser.add_argument("-f", action="store")
	parser.add_argument("-i", action="store")

	args = parser.parse_args()

	run(args.t, args.f, args.i)
