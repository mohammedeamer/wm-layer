import math

import torch as tch
import torch.nn.functional as F

from config import CONFIG

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, out_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.out_chs = out_chs

		self.out_dim = int(((float(in_dim) - float(field))/float(step)) + 1.)

		self.weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, self.out_dim, self.out_dim))

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_chs*self.field*self.field)

		self.weights.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		kernel = tch.ones(self.in_chs, 1, self.field, self.field)

		if tch.cuda.is_available():
			kernel = kernel.cuda()

		output = F.conv2d(x, kernel, stride=self.step, groups=self.in_chs)

		# (batch, out_chs, in_dim, in_dim)
		output = (self.weights.unsqueeze(0) * output.unsqueeze(1)).sum(dim=2)

		if self.bias is not None:
			output = output + self.bias

		return output

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
