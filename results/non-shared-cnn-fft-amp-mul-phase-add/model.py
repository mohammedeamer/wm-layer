import math

import torch as tch

from config import CONFIG

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, out_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.out_chs = out_chs

		self.spectrum_dim = int(in_dim/2.0)+1

		self.amp_weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, self.spectrum_dim))
		self.phase_weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, self.spectrum_dim))

		self.out_dim = int(((float(in_dim) - float(field))/float(step)) + 1.)
		self.pad = int((in_dim - self.out_dim)/2.0)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_chs*self.field*self.field)

		self.amp_weights.data.uniform_(-stdv, stdv)
		self.phase_weights.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		spec = tch.rfft(x, 2, onesided=True)

		amp = tch.sqrt((spec**2).sum(dim=-1))
		phase = tch.atan2(spec[...,1], spec[...,0])

		# (batch, out_chs, in_dim, in_dim/2-1)
		amp_o = amp.unsqueeze(1)*self.amp_weights.unsqueeze(0)

		# (batch, out_chs, in_chs, in_dim, in_dim/2-1)
		phase_o = phase.unsqueeze(1)+self.phase_weights.unsqueeze(0)

		re = (amp_o*tch.cos(phase_o))[...,:-2*self.pad,:-self.pad].sum(dim=2)
		img = (amp_o*tch.sin(phase_o))[...,:-2*self.pad,:-self.pad].sum(dim=2)

		# (batch, out_chs, in_dim, in_dim)
		output = tch.irfft(tch.stack([re, img], dim=-1), 2, signal_sizes=(self.out_dim, self.out_dim), onesided=True)

		if self.bias is not None:
			output = output + self.bias

		output = tch.sin(output)

		return output

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))


		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
