
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_channels':1, 'kernel_size':3, 'stride':1, 'out_channels':144},
			{'in_channels':144, 'kernel_size':3, 'stride':1, 'out_channels':168},
			{'in_channels':168, 'kernel_size':3, 'stride':1, 'out_channels':1}],
	'linear_layers': [{'in_features':22*22, 'out_features':10}]
}


