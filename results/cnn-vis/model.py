import torch as tch

class CNN(tch.nn.Module):

	def __init__(self, CONFIG):

		super(CNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(tch.nn.Conv2d(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))


		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x, conv_index):

		h = self.conv[:conv_index*2+2](x)
		o = self.conv[conv_index*2+2:](h)
		return self.linear(o.view(x.size(0), -1)), h
