import math

import torch as tch
import torch.nn.functional as F

class RFConv2d(tch.nn.Module):

	def __init__(self, in_channels ,out_channels, in_dim, kernel_size, stride, padding, bias=True):

		super(RFConv2d, self).__init__()

		self.in_channels = in_channels
		self.in_dim = in_dim
		self.kernel_size = kernel_size
		self.stride = stride
		self.out_channels = out_channels

		self.padding = padding

		self.weight = tch.nn.Parameter(tch.FloatTensor(out_channels, in_channels, in_dim, in_dim))

		self.out_dim = int(((in_dim + 2.0*padding - kernel_size)/stride) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(out_channels, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_channels*self.kernel_size**2)

		self.weight.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		# (batch, out_chs, in_chs, in_dim, in_dim)
		scaled = (self.weight.unsqueeze(0) * x.unsqueeze(1))

		output = self.kernel_size**2*F.avg_pool2d(scaled.view(x.size(0), -1, self.in_dim, self.in_dim),
						kernel_size=self.kernel_size, stride=self.stride, padding=self.padding)


		crop_start = int((self.in_dim - self.out_dim)/2)
		crop_end = crop_start+self.out_dim

		scaled_crop = scaled[...,crop_start:crop_end,crop_start:crop_end]

		output = output.view_as(scaled_crop)

		output = 2*scaled_crop - output

		output = output.sum(dim=2)

		if self.bias is not None:
			output = output + self.bias

		return output

class DenseBlock(tch.nn.Module):

	def __init__(self, in_channels, growth_rate, in_dim, layers_num):

		super(DenseBlock, self).__init__()

		layer_in_chs = in_channels

		self.main = []

		for _ in range(layers_num):

			self.main.append(tch.nn.Sequential(RFConv2d(layer_in_chs, growth_rate, in_dim, kernel_size=3, padding=1, stride=1),
											tch.nn.ReLU()))

			layer_in_chs += growth_rate

		self.main = tch.nn.ModuleList(self.main)

	def forward(self, x):

		o = x

		for mod in self.main:

			h = mod(o)
			o = tch.cat([o, h], dim=1)

		return o

class DenseNet(tch.nn.Module):

	def __init__(self):

		super(DenseNet, self).__init__()

		self.main = [tch.nn.Conv2d(1, 16, kernel_size=3, padding=1, stride=1),
				tch.nn.ReLU()]

		self.main.append(DenseBlock(in_channels=16, growth_rate=8, in_dim=28, layers_num=2))
		self.main.append(tch.nn.MaxPool2d(2, stride=2))

		self.main.append(DenseBlock(in_channels=32, growth_rate=8, in_dim=14, layers_num=4))
		self.main.append(tch.nn.MaxPool2d(2, stride=2))

		self.main.append(DenseBlock(in_channels=64, growth_rate=8, in_dim=7, layers_num=8))
		self.main.append(tch.nn.MaxPool2d(2, stride=2))

		self.main.append(DenseBlock(in_channels=128, growth_rate=8, in_dim=3, layers_num=16))

		self.main.append(tch.nn.AvgPool2d(3))

		self.main = tch.nn.Sequential(*self.main)

		self.out = tch.nn.Linear(256, 10)

	def forward(self, x):

		o = self.main(x)
		return self.out(o.view(x.size(0), -1))
