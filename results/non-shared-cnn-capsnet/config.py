
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'capsnet':{
				'conv_layer': {'in_chs':1, 'in_dim':28, 'out_chs':51, 'field':9, 'step':1},
				'prim_caps': {'in_channels':51, 'in_dim':20, 'kernel_size':9, 'stride':2, 'outcaps_num':32, 'outcaps_dim':8},
				'digit_caps': {'incaps_num':32*6*6, 'incaps_dim':8, 'outcaps_num':10, 'outcaps_dim':16}
			}
}


