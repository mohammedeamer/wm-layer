import torch as tch
import torch.nn.functional as F

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, out_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.out_chs = out_chs

		self.weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, in_dim))

		self.out_dim = int(((float(in_dim) - float(field))/float(step)) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		self.weights.data.uniform_(-1.0, 1.0)

		if self.bias is not None:
			self.bias.data.uniform_(-1.0, 1.0)

	def forward(self, x):

		# (batch, in_chs, in_dim, in_dim, out_chs)
		scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1)).view(x.size(0), -1, self.in_dim, self.in_dim)

		kernel = tch.ones(self.out_chs, self.in_chs, self.field, self.field)

		if tch.cuda.is_available():
			kernel = kernel.cuda()

		output = F.conv2d(scaled, kernel, stride=self.step, groups=self.out_chs)

		if self.bias is not None:
			output = output + self.bias

		return output
