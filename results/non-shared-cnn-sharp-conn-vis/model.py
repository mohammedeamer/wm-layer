import math

import torch as tch
import torch.nn.functional as F

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, out_chs, bias):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.out_chs = out_chs

		padding = math.ceil((step*(in_dim - 1) - in_dim + field)/2)

		self.padding = padding

		self.weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, in_dim))

		self.out_dim = int(((in_dim + 2.0*padding - field)/step) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_chs*self.field**2)

		self.weights.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		# (batch, out_chs, in_chs, in_dim, in_dim)
		scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1))

		output = self.field**2*F.avg_pool2d(scaled.view(x.size(0), -1, self.in_dim, self.in_dim),
						kernel_size=self.field, stride=self.step, padding=self.padding)

		output = output.view_as(scaled)
		output = 2*scaled - output

		output = output.sum(dim=2)

		if self.bias is not None:
			output = output + self.bias

		return output

class NSCNN(tch.nn.Module):

	def __init__(self, CONFIG):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x, conv_index):

		h = self.conv[:conv_index*2+2](x)
		o = self.conv[conv_index*2+2:](h)
		return self.linear(o.view(x.size(0), -1)), h
