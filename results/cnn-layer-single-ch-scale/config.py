
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'scaled_layer': {'idx': 0, 'out_dim': 26, 'out_chs': 33},
	'conv_layers':[{'in_channels':1, 'kernel_size':3, 'stride':1, 'padding':0, 'out_channels':33},
			{'in_channels':33, 'kernel_size':3, 'stride':1, 'padding':0, 'out_channels':33},
			{'in_channels':33, 'kernel_size':3, 'stride':1, 'padding':0,  'out_channels':8}],
	'linear_layers': [{'in_features':8*22*22, 'out_features':64}, {'in_features':64, 'out_features':10}]
}

