from __future__ import print_function
import os, csv, json

import torch as tch

def disp(txt, ongoing=False):
	if ongoing:
		print(txt, end="\r", flush=True)
	else:
		print(txt, flush=True)

class Logger:

	def __init__(self, path=None):

		if path is not None:
			self.init(path)

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		self.close()

	def __write_rows(self, tag, rows):

		with open(os.path.join(self.path, "{}.csv".format(tag)), "a") as f:
			csv_writer = csv.writer(f)
			csv_writer.writerows(rows)

	def init(self, path):
		self.path = path

	def log_train_perform(self, epoch, train_perform, valid_perform):
		self.__write_rows("train_perform", [[epoch, train_perform, valid_perform]])

	def log_train_loss(self, epoch, train_loss):
		self.__write_rows("train_loss", [[epoch, train_loss]])

	def log_pred(self, preds):
		self.__write_rows("preds", preds)

	def log_test_perform(self, test_perform):
		self.__write_rows("test_error", [[test_perform]])

	def save_model(self, model):
		tch.save(model.state_dict(), os.path.join(self.path,"model.pt"))

	def save_config(self, config):
		with open(os.path.join(self.path, "config.json"), 'w') as f:
			json.dump(config, f)

MainLogger = Logger()
