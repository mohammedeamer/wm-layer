import numpy as np
import torch as tch
import torch.nn.functional as F

from config import CONFIG

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, out_dim, step, out_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.out_dim = out_dim
		self.step = step
		self.out_chs = out_chs

		self.padding = int(np.ceil((step*(out_dim-1))/2.0))

		self.weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, in_dim))

		#self.bias = None

		#if bias:
			#self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		self.weights.data.uniform_(-1.0, 1.0)

		#if self.bias is not None:
			#self.bias.data.uniform_(-1.0, 1.0)

	def forward(self, x):

		# (batch, out_chs, in_chs, in_dim, in_dim)
		scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1))

		# (batch, out_chs, in_dim, in_dim)
		scaled = scaled.sum(dim=2)

		# (1, batch*out_chs, in_dim, in_dim)
		scaled = scaled.view(-1, self.in_dim, self.in_dim).unsqueeze(0)

		# (batch*out_chs, 1, in_dim, in_dim)
		kernel = scaled.view(-1, self.in_dim, self.in_dim).unsqueeze(1)

		# (1, batch*out_chs, out_dim, out_dim)
		output = F.conv2d(scaled, kernel, stride=self.step, padding=self.padding, groups=kernel.size(0))

		#if self.bias is not None:
			#output = output + self.bias

		return output.squeeze(0).view(x.size(0), self.out_chs, self.out_dim, self.out_dim)

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
