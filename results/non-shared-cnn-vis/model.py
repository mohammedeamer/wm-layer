import torch as tch

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, out_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.out_chs = out_chs

		self.weights = tch.nn.Parameter(tch.FloatTensor(in_chs, in_dim, in_dim, out_chs))

		self.out_dim = int(((float(in_dim) - float(field))/float(step)) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		self.weights.data.normal_()

		if self.bias is not None:
			self.bias.data.normal_()

	def forward(self, x):

		# (batch, in_chs, in_dim, in_dim, out_chs)
		scaled = self.weights.unsqueeze(0) * x.unsqueeze(-1)

		w_output = []

		for i in range(self.out_dim):

				o = scaled[:, :, i*self.step:i*self.step+self.field].sum(dim=1).sum(dim=1)

				w_output.append(o)

		# (batch, out_dim, in_dim, out_chs)
		w_output = tch.stack(w_output, dim=1)

		output = []

		for i in range(self.out_dim):

				o = w_output[:,:,i*self.step:i*self.step+self.field].sum(dim=2)

				output.append(o)

		# (batch, out_chs, out_dim, out_dim)
		output =  tch.stack(output, dim=2).transpose(2, 3).transpose(1, 2)

		if self.bias is not None:
			output = output + self.bias

		return output

class NSCNN(tch.nn.Module):

	def __init__(self, CONFIG):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))


		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x, conv_index):

		h = self.conv[:conv_index*2+2](x)
		o = self.conv[conv_index*2+2:](h)

		pre_kernel = x

		if conv_index > 0:
			pre_kernel = self.conv[:(conv_index-1)*2+2](x)

		pre_kernel = pre_kernel.unsqueeze(-1) * self.conv[conv_index*2].weights.unsqueeze(0)

		return self.linear(o.view(x.size(0), -1)), h, pre_kernel
