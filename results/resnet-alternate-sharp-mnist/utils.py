import os

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms


class AverageMeter(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


NUM_WORKERS = 2

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/mnist/exp-data")


def get_ds():

    train_trans = transforms.Compose([transforms.Pad(2),
                                      transforms.RandomCrop(28),
                                      transforms.ToTensor()])
    valid_trans = transforms.Compose([transforms.ToTensor()])

    train_ds = dset.MNIST(root=ds_path,
                          train=True, transform=train_trans, download=True)

    train_ds = dutils.Subset(train_ds, range(54000))

    valid_ds = dset.MNIST(root=ds_path,
                          train=True, transform=valid_trans, download=True)

    valid_ds = dutils.Subset(valid_ds, range(54000, 60000))

    train_ds = dutils.DataLoader(train_ds, batch_size=128, shuffle=True)
    valid_ds = dutils.DataLoader(valid_ds, batch_size=256, shuffle=True)

    test_ds = dset.MNIST(root=ds_path,
                         train=False, transform=valid_trans, download=True)

    test_ds = dutils.DataLoader(test_ds, shuffle=True, batch_size=256)

    return train_ds, valid_ds, test_ds


def load_model(model, trial_results_path):

    model_path = os.path.join(trial_results_path, 'model.pt')

    model.load_state_dict(tch.load(model_path))
