import sys
import os
import argparse
import logging

import torch as tch

from logger import MainLogger as disklog
import train_helpers as helpers
from model import ResNet
from config import CONFIG
from utils import get_ds, load_model

dir_path = os.path.dirname(os.path.realpath(__file__))

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format=log_format, datefmt='%m/%d %I:%M:%S %p')


device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')


def predictions(o):
    return o.argmax(dim=-1)


def perform_fn(o, y):

    preds = predictions(o)
    return (preds != y).float().mean()


def train(tag):

    results_path = os.path.join(dir_path, "exp-results", tag)

    os.makedirs(results_path)

    disklog.init(results_path)

    disklog.save_config(CONFIG)

    trials = CONFIG["exp_params"]["train_trials"]
    epochs = CONFIG["exp_params"]["train_epochs"]

    train_ds, valid_ds, test_ds = get_ds()

    for t in range(1, trials + 1):

        trial_results_path = os.path.join(results_path, str(t))

        os.makedirs(trial_results_path)

        disklog.init(trial_results_path)

        model = ResNet().to(device)

        helpers.train(model=model, loss_fn=tch.nn.CrossEntropyLoss(),
                      perform_fn=perform_fn,
                      optim_fn=tch.optim.Adam, train_ds=train_ds,
                      valid_ds=valid_ds, epochs=epochs,
                      results_path=trial_results_path)

        load_model(model, trial_results_path)

        test_err = helpers.infer(
            model, perform_fn=perform_fn, valid_ds=test_ds)

        disklog.log_test_perform(test_err)


SUBEXPS = {
    "train": train,
}

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("subexp", action="store")
    parser.add_argument("tag", action="store")

    args = parser.parse_args()

    SUBEXPS[args.subexp](args.tag)
