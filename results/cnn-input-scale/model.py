import torch as tch

from config import CONFIG

class CNN(tch.nn.Module):

	def __init__(self):

		super(CNN, self).__init__()

		self.weights = tch.nn.Parameter(tch.FloatTensor(1, 28, 28))
		self.weights.data.uniform_(-1,1)

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(tch.nn.Conv2d(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))


		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = x*self.weights.unsqueeze(0)

		o = self.conv(o)

		return self.linear(o.view(x.size(0), -1))
