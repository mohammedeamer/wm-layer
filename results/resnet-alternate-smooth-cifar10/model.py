import math

import torch as tch
import torch.nn.functional as F


class RFConv2d(tch.nn.Module):

    def __init__(self, in_channels, out_channels, in_dim, kernel_size, stride,
                 padding, bias=True):

        super(RFConv2d, self).__init__()

        self.in_channels = in_channels
        self.in_dim = in_dim
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.out_channels = out_channels

        self.weights = tch.nn.Parameter(tch.FloatTensor(
            out_channels, in_channels, in_dim, in_dim))

        self.out_dim = int(
            ((in_dim + 2.0 * padding - kernel_size) / stride) + 1.)

        self.bias = None

        if bias:
            self.bias = tch.nn.Parameter(tch.FloatTensor(
                out_channels, self.out_dim, self.out_dim))

        self.reset_parameters()

    def reset_parameters(self):

        stdv = 1. / math.sqrt(self.in_channels
                              * self.kernel_size * self.kernel_size)

        self.weights.data.uniform_(-stdv, stdv)

        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, x):

        # (batch, in_chs, in_dim, in_dim, out_chs)
        scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1)
                  ).view(x.size(0), -1, self.in_dim, self.in_dim)

        kernel = tch.ones(self.out_channels, self.in_channels,
                          self.kernel_size, self.kernel_size)

        if tch.cuda.is_available():
            kernel = kernel.cuda()

        output = F.conv2d(scaled, kernel, stride=self.stride,
                          padding=self.padding, groups=self.out_channels)

        if self.bias is not None:
            output = output + self.bias

        return output


class ResBlock(tch.nn.Module):

    def __init__(self, in_channels, out_channels, in_dim, down_sample):

        super(ResBlock, self).__init__()

        self.down_sample = down_sample

        first_stride = 2 if down_sample else 1

        second_in_dim = in_dim

        if down_sample:
            second_in_dim = int((in_dim - 1) / 2. + 1)

        self.apply_proj = down_sample or (in_channels != out_channels)

        self.main = tch.nn.Sequential(
            tch.nn.Conv2d(in_channels, out_channels,
                          kernel_size=3, padding=1, stride=first_stride),
            tch.nn.ReLU(),
            RFConv2d(
                out_channels, out_channels, second_in_dim, kernel_size=3,
                padding=1, stride=1),
            tch.nn.ReLU())

        if self.apply_proj:
            self.proj = tch.nn.Conv2d(
                in_channels, out_channels, kernel_size=1, stride=first_stride)

    def forward(self, x):

        o = self.main(x)

        if self.apply_proj:
            x = self.proj(x)

        return o + x


class ResNet(tch.nn.Module):

    def __init__(self):

        super(ResNet, self).__init__()

        self.main = list()

        in_chs = 3
        in_dim = 32
        down_sample = False
        for _ in range(3):

            self.main.append(ResBlock(in_chs, 8, in_dim, down_sample))
            in_chs = 8

        down_sample = True
        for _ in range(4):

            self.main.append(ResBlock(in_chs, 16, in_dim, down_sample))
            in_chs = 16
            in_dim = 16
            down_sample = False

        down_sample = True
        for _ in range(6):

            self.main.append(ResBlock(in_chs, 32, in_dim, down_sample))
            in_chs = 32
            in_dim = 8
            down_sample = False

        down_sample = True
        for _ in range(3):

            self.main.append(ResBlock(in_chs, 64, in_dim, down_sample))
            in_chs = 64
            in_dim = 4
            down_sample = False

        self.main.append(tch.nn.AvgPool2d(4))

        self.main = tch.nn.Sequential(*self.main)

        self.out = tch.nn.Linear(64, 10)

    def forward(self, x):

        h = self.main[:-1](x)
        o = self.main[-1](h)

        return self.out(o.view(x.size(0), -1)), h
