
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'rf_layer': {'in_chs':1, 'in_dim':28, 'field':3, 'step':1, 'padding':0, 'out_chs':32},
	'conv_layers':[{'in_channels':32, 'kernel_size':3, 'stride':1, 'padding':0, 'out_channels':32},
			{'in_channels':32, 'kernel_size':3, 'stride':1, 'padding':0,  'out_channels':8}],
	'linear_layers': [{'in_features':8*22*22, 'out_features':64}, {'in_features':64, 'out_features':10}]
}

