- DenseNet with alternating WM layer
- Fixed kernel
- Sharp WM layer
- Last hidden state is returned for later visualization
