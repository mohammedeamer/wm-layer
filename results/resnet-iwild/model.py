import torch as tch


class ResBlock(tch.nn.Module):

    def __init__(self, in_channels, out_channels, down_sample):

        super(ResBlock, self).__init__()

        self.down_sample = down_sample

        first_stride = 2 if down_sample else 1

        self.apply_proj = down_sample or (in_channels != out_channels)

        self.main = tch.nn.Sequential(tch.nn.Conv2d(in_channels, out_channels,
                                                    kernel_size=3, padding=1,
                                                    stride=first_stride),
                                      tch.nn.ReLU(),
                                      tch.nn.Conv2d(
                                          out_channels, out_channels,
                                          kernel_size=3, padding=1, stride=1),
                                      tch.nn.ReLU())

        if self.apply_proj:
            self.proj = tch.nn.Conv2d(
                in_channels, out_channels, kernel_size=1, stride=first_stride)

    def forward(self, x):

        o = self.main(x)

        if self.apply_proj:
            x = self.proj(x)

        return o + x


class ResNet(tch.nn.Module):

    def __init__(self):

        super(ResNet, self).__init__()

        self.main = list()

        in_chs = 3
        down_sample = False
        for _ in range(3):

            self.main.append(ResBlock(in_chs, 8, down_sample))
            in_chs = 8

        down_sample = True
        for _ in range(4):

            self.main.append(ResBlock(in_chs, 16, down_sample))
            in_chs = 16
            down_sample = False

        down_sample = True
        for _ in range(6):

            self.main.append(ResBlock(in_chs, 32, down_sample))
            in_chs = 32
            down_sample = False

        down_sample = True
        for _ in range(3):

            self.main.append(ResBlock(in_chs, 64, down_sample))
            in_chs = 64
            down_sample = False

        self.main.append(tch.nn.AvgPool2d(4))

        self.main = tch.nn.Sequential(*self.main)

        self.out = tch.nn.Linear(64, 10)

    def forward(self, x):

        h = self.main[:-1](x)

        o = self.main[-1](h)

        return self.out(o.view(x.size(0), -1)), h
