import sys
import os
import csv
import argparse

import torch as tch

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(dir_path, '..'))

results_path = os.path.join(dir_path, 'script-exp-results/adv-norm/')


def load_model(model, trial_results_path):

    model_path = os.path.join(trial_results_path, 'model.pt')

    model.load_state_dict(tch.load(model_path, map_location=device))


def log_eps_results(results, tag):

    with open(os.path.join(results_path, '{}.csv'.format(tag)), 'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(results)


def get_model(model_path):

    from model import ResNet

    model = ResNet().to(device)

    load_model(model, model_path)

    return model


def test_model(model, ds, eps=None):

    from run import perform_fn
    from utils import AverageMeter

    loss_fn = tch.nn.CrossEntropyLoss()

    model.eval()

    batches_num = len(ds)

    test_avg = AverageMeter()
    confid = 0.0

    for b, (x, y) in enumerate(ds):

        x = x.to(device)
        y = y.to(device)

        if eps is not None:
            x.requires_grad_()

        o, _ = model(x)

        if eps is not None:

            loss = loss_fn(o, y)

            x = x + eps * tch.autograd.grad([loss], x)[0].sign()

            x_flat = x.reshape(x.size(0), x.size(1), -1)
            x_flat_min = x_flat.min(dim=-1, keepdim=True)[0]
            x_flat_max = x_flat.max(dim=-1, keepdim=True)[0]

            x_flat = (x_flat - x_flat_min) / (
                x_flat_max - x_flat_min)

            x = x_flat.reshape_as(x)

            o, _ = model(x)

        perform = perform_fn(o, y)
        test_avg.update(perform.item(), x.size(0))

        confid += tch.softmax(o, dim=-1).max(dim=-1)[0].sum(dim=0).item()

        print('eps: {} - {}/{}'.format(eps, b + 1, batches_num))

    confid = confid / len(ds.dataset)

    return test_avg.avg, confid


def run(tag, model_path):

    from utils import get_ds

    os.makedirs(results_path)

    _, _, test_ds = get_ds()

    model = get_model(model_path)

    epsilons = [None, 0.01, 0.05, 0.1, 0.2,
                0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.]

    err_arr = []
    confid_arr = []

    for eps in epsilons:

        err, confid = test_model(model, test_ds, eps=eps)

        err_arr.append(err)
        confid_arr.append(confid)

    results = list(zip(epsilons, err_arr, confid_arr))

    log_eps_results(results, tag)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("tag", action="store")
    parser.add_argument("model_path", action="store")

    args = parser.parse_args()

    run(args.tag, args.model_path)
