import os, importlib.util, json, sys

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

tch.set_grad_enabled(False)

dir_path = os.path.dirname(os.path.realpath(__file__))

results_path = os.path.join(dir_path, '../exp-results/noise-norm/')

os.makedirs(results_path)

MODELS = {
					'cnn': '../',
					'cnn-batchnorm': '../../cnn-batchnorm/',
					'non-shared-cnn-v3': '../../non-shared-cnn-v3/',
					'rf-conv-cnn': '../../rf-conv-cnn/',
					'non-shared-cnn-batchnorm': '../../non-shared-cnn-batchnorm/',
					'rf-conv-cnn-batchnorm': '../../rf-conv-cnn-batchnorm/'
				}

PARAMS = {
					'cnn': '../exp-results/cnn-conv[33-33-8]-linear[64-10]/1/2/model.pt',
					'cnn-batchnorm': '../../cnn-batchnorm/exp-results/cnn-batchnorm-conv[33-33-8]-linear[64-10]/1/2/model.pt',
					'non-shared-cnn-v3': '../../non-shared-cnn-v3/exp-results/non-shared-cnn-v3-conv[3-32]-linear[64-10]/1/1/model.pt',
					'rf-conv-cnn': '../../rf-conv-cnn/exp-results/rf-conv-cnn[3-32]-linear[64-10]/1/3/model.pt',
					'non-shared-cnn-batchnorm': '../../non-shared-cnn-batchnorm/exp-results/non-shared-cnn-batchnorm-conv[32-32-8]-linear[64-10]/1/2/model.pt',
					'rf-conv-cnn-batchnorm': '../../rf-conv-cnn-batchnorm/exp-results/rf-conv-cnn-batchnorm-conv[32-32-8]-linear[64-10]/1/1/model.pt'
				}

def load_model(class_path, params_path):

	sys.path.insert(0, class_path)

	spec = importlib.util.spec_from_file_location("model", os.path.join(class_path, 'model.py'))
	model = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(model)

	if 'config' in sys.modules.keys():
		del sys.modules['config']


	try:
		m = model.CNN()
	except:
		m = model.NSCNN()

	m.load_state_dict(tch.load(params_path, map_location=lambda storage, loc: storage))

	sys.path.remove(class_path)

	return m

def test_model(model, ds, uniform_noise=None, trials=1):

    device = tch.device("cuda" if tch.cuda.is_available() else "cpu")

    model.to(device)
    model.eval()

    points_num = len(ds)

    ds_loader = dutils.DataLoader(ds, batch_size=64)

    batches_num = len(ds_loader)

    test_errs = []

    for t in range(1,trials+1):

        test_err = 0.0

        for b, (X, y) in enumerate(ds_loader):

            X = X.to(device)
            y = y.to(device)

            if uniform_noise is not None:

                noise = tch.empty_like(X).uniform_(*uniform_noise).to(device)
                X = X + noise

                X_flat = X.view(X.size(0), X.size(1), -1)
                X_flat = (X_flat - X_flat.min(dim=-1, keepdim=True)[0])/(X_flat.max(dim=-1, keepdim=True)[0] - X_flat.min(dim=-1, keepdim=True)[0])

                X = X_flat.view_as(X)

            o = model(X).argmax(dim=-1)
            test_err += (o != y).sum().item()

            print('trial: {} - {}/{}'.format(t, b+1, batches_num), end='\r')

        test_err = test_err/points_num
        test_errs.append(test_err)

    return test_errs

def test_model_multiple_uniform(model, ds):

    errs = {}

    noise_ranges = [None, (0., 0.01), (0., 0.05),
                    (0., 0.1), (0., 0.2), (0., 0.3), (0., 0.4),
                    (0., 0.5), (0., 0.6), (0., 0.7), (0., 0.8), (0., 0.9), (0., 1.)]

    for noise in noise_ranges:

        if noise is None:

            errs['None'] = test_model(model, ds, uniform_noise=None, trials=1)
            continue

        err = test_model(model, ds, uniform_noise=noise, trials=3)
        errs[str(noise)] = err

    return errs

def run():

	mnist_path = os.path.join(dir_path, '../../../data/mnist/exp-data')
	mnist_test = dset.MNIST(root=mnist_path, train=False, download=True, transform=transforms.ToTensor())

	for k in MODELS.keys():

		class_path = os.path.join(dir_path, MODELS[k])
		params_path = os.path.join(dir_path, PARAMS[k])

		model = load_model(class_path=class_path, params_path=params_path)

		noise_test_errs = test_model_multiple_uniform(model, mnist_test)

		with open(os.path.join(results_path, '{}.json'.format(k)), 'w') as f:
			json.dump(noise_test_errs, f)


if __name__ == '__main__':
	run()
