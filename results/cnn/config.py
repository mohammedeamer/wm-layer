
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_channels':1, 'kernel_size':9, 'stride':1, 'padding':3, 'out_channels':33},
			{'in_channels':33, 'kernel_size':9, 'stride':1, 'padding':3, 'out_channels':33},
			{'in_channels':33, 'kernel_size':9, 'stride':1, 'padding':3,  'out_channels':8}],
	'linear_layers': [{'in_features':8*22*22, 'out_features':64}, {'in_features':64, 'out_features':10}]
}

