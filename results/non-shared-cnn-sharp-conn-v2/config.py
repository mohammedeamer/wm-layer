
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_channels':1, 'in_dim':28, 'kernel_size':3, 'stride':1, 'padding': 0, 'out_channels':128, 'bias': True},
			{'in_channels':128, 'in_dim':26, 'kernel_size':3, 'stride':1, 'padding': 0, 'out_channels':128, 'bias': True},
			{'in_channels':128, 'in_dim':24, 'kernel_size':3, 'stride':1, 'padding': 0, 'out_channels':128, 'bias': True},
			{'in_channels':128, 'in_dim':22, 'kernel_size':3, 'stride':1, 'padding': 0, 'out_channels':32, 'bias': True}],
	'linear_layers': [{'in_features':32*20*20, 'out_features':256}, {'in_features':256, 'out_features':10}]
}


