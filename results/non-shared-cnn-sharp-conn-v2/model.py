import math

import torch as tch
import torch.nn.functional as F

from config import CONFIG

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_channels ,out_channels, in_dim, kernel_size, stride, padding, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_channels = in_channels
		self.in_dim = in_dim
		self.kernel_size = kernel_size
		self.stride = stride
		self.out_channels = out_channels

		self.padding = padding

		self.weight = tch.nn.Parameter(tch.FloatTensor(out_channels, in_channels, in_dim, in_dim))

		self.out_dim = int(((in_dim + 2.0*padding - kernel_size)/stride) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(out_channels, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_channels*self.kernel_size**2)

		self.weight.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		# (batch, out_chs, in_chs, in_dim, in_dim)
		scaled = (self.weight.unsqueeze(0) * x.unsqueeze(1))

		output = self.kernel_size**2*F.avg_pool2d(scaled.view(x.size(0), -1, self.in_dim, self.in_dim),
						kernel_size=self.kernel_size, stride=self.stride, padding=self.padding)


		crop_start = int((self.in_dim - self.out_dim)/2)
		crop_end = crop_start+self.out_dim

		scaled_crop = scaled[...,crop_start:crop_end,crop_start:crop_end]

		output = output.view_as(scaled_crop)

		output = 2*scaled_crop - output

		output = output.sum(dim=2)

		if self.bias is not None:
			output = output + self.bias

		return output

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
