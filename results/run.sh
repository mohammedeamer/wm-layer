#!/bin/bash

set -e

python3 $1 "${@:2}"
