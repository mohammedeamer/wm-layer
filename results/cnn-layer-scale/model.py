import torch as tch

class CNN(tch.nn.Module):

	def __init__(self, CONFIG):

		super(CNN, self).__init__()

		scaled_layer_cfg = CONFIG['scaled_layer']

		self.scaled_layer_idx = 2*(scaled_layer_cfg['idx']+1)

		self.scaled_layer_out_dim = scaled_layer_cfg['out_dim']
		self.scaled_layer_out_chs = scaled_layer_cfg['out_chs']

		self.weights = tch.nn.Parameter(tch.FloatTensor(self.scaled_layer_out_chs, self.scaled_layer_out_dim, self.scaled_layer_out_dim))
		self.weights.data.uniform_(-1,1)

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(tch.nn.Conv2d(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv[:self.scaled_layer_idx](x)

		o = o*self.weights.unsqueeze(0)

		o = self.conv[self.scaled_layer_idx:](o)

		return self.linear(o.view(x.size(0), -1))
