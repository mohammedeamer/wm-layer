import os, importlib.util, json, sys

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

tch.set_grad_enabled(False)

dir_path = os.path.dirname(os.path.realpath(__file__))

results_path = os.path.join(dir_path, '../exp-results/noise-norm-mse/')

os.makedirs(results_path)

MODELS = {
				'skip-cnn': '../../skip-cnn-vis/',
				'skip-fixed-kernel-sharp-conn': '../../skip-fixed-kernel-sharp-conn-vis/',
				'skip-fixed-kernel-sharp-conn-alternate': '../../skip-fixed-kernel-sharp-conn-alternate-vis/',
				}

PARAMS = {
				'skip-cnn': '../../skip-cnn/exp-results/skip-cnn/1/1/model.pt',
				'skip-fixed-kernel-sharp-conn': '../../skip-fixed-kernel-sharp-conn/exp-results/skip-fixed-kernel-sharp-conn/1/3/model.pt',
				'skip-fixed-kernel-sharp-conn-alternate': '../../skip-fixed-kernel-sharp-conn-alternate/exp-results/skip-fixed-kernel-sharp-conn-alternate/1/3/model.pt'
				}

CONFIGS = {
				'skip-cnn': '../../skip-cnn/exp-results/skip-cnn/1/config.json',
				'skip-fixed-kernel-sharp-conn': '../../skip-fixed-kernel-sharp-conn/exp-results/skip-fixed-kernel-sharp-conn/1/config.json',
				'skip-fixed-kernel-sharp-conn-alternate': '../../skip-fixed-kernel-sharp-conn-alternate/exp-results/skip-fixed-kernel-sharp-conn-alternate/1/config.json'
				}

def load_model(class_path, params_path, config_path):

	sys.path.insert(0, class_path)

	spec = importlib.util.spec_from_file_location("model", os.path.join(class_path, 'model.py'))
	model = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(model)

	if 'config' in sys.modules.keys():
			del sys.modules['config']

	m = model.DenseNet()

	m.load_state_dict(tch.load(params_path, map_location=lambda storage, loc: storage))

	sys.path.remove(class_path)

	return m

def test_model(model, ds, uniform_noise=None, trials=1):

		device = tch.device("cuda" if tch.cuda.is_available() else "cpu")

		model.to(device)
		model.eval()

		points_num = len(ds)

		ds_loader = dutils.DataLoader(ds, batch_size=64)

		batches_num = len(ds_loader)

		test_errs = []

		for t in range(1,trials+1):

				test_err = 0.0

				for b, (X, y) in enumerate(ds_loader):

						b_size = X.size(0)

						X = X.to(device)
						y = y.to(device)

						o, h = model(X)

						noise = tch.empty_like(X).uniform_(*uniform_noise).to(device)
						X_ = X + noise

						X_flat = X_.view(X_.size(0), X_.size(1), -1)
						X_flat = (X_flat - X_flat.min(dim=-1, keepdim=True)[0])/(X_flat.max(dim=-1, keepdim=True)[0] - X_flat.min(dim=-1, keepdim=True)[0])

						X_ = X_flat.view_as(X_)

						o_, h_ = model(X_)

						test_err += tch.nn.MSELoss()(h, h_).item() * b_size

						print('trial: {} - {}/{}'.format(t, b+1, batches_num), end='\r')

				test_err = test_err/points_num
				test_errs.append(test_err)

		return test_errs

def test_model_multiple_uniform(model, ds):

		errs = {}

		noise_ranges = [(0., 0.01), (0., 0.05),
						(0., 0.1), (0., 0.2), (0., 0.3), (0., 0.4),
						(0., 0.5), (0., 0.6), (0., 0.7), (0., 0.8), (0., 0.9), (0., 1.)]

		for noise in noise_ranges:

				if noise is None:

						errs['None'] = test_model(model, ds, uniform_noise=None, trials=1)
						continue

				err = test_model(model, ds, uniform_noise=noise, trials=3)
				errs[str(noise)] = err

		return errs

def run():

	mnist_path = os.path.join(dir_path, '../../../data/mnist/exp-data')
	mnist_test = dset.MNIST(root=mnist_path, train=False, download=True, transform=transforms.ToTensor())

	for k in MODELS.keys():

		class_path = os.path.join(dir_path, MODELS[k])
		params_path = os.path.join(dir_path, PARAMS[k])
		config_path = os.path.join(dir_path, CONFIGS[k])

		model = load_model(class_path=class_path, params_path=params_path, config_path=config_path)

		noise_test_errs = test_model_multiple_uniform(model, mnist_test)

		with open(os.path.join(results_path, '{}.json'.format(k)), 'w') as f:
				json.dump(noise_test_errs, f)


if __name__ == '__main__':
		run()
