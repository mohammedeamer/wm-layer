import torch as tch


class DenseBlock(tch.nn.Module):

    def __init__(self, in_channels, growth_rate, layers_num):

        super(DenseBlock, self).__init__()

        layer_in_chs = in_channels

        self.main = []

        for _ in range(layers_num):

            self.main.append(tch.nn.Sequential(
                tch.nn.Conv2d(layer_in_chs, growth_rate,
                              kernel_size=3, padding=1, stride=1),
                tch.nn.ReLU()))

            layer_in_chs += growth_rate

        self.main = tch.nn.ModuleList(self.main)

    def forward(self, x):

        o = x

        for mod in self.main:

            h = mod(o)
            o = tch.cat([o, h], dim=1)

        return o


class DenseNet(tch.nn.Module):

    def __init__(self):

        super(DenseNet, self).__init__()

        self.main = [tch.nn.Conv2d(3, 16, kernel_size=3, padding=1, stride=1),
                     tch.nn.ReLU()]

        self.main.append(DenseBlock(
            in_channels=16, growth_rate=8, layers_num=2))
        self.main.append(tch.nn.MaxPool2d(2, stride=2))

        self.main.append(DenseBlock(
            in_channels=32, growth_rate=8, layers_num=4))
        self.main.append(tch.nn.MaxPool2d(2, stride=2))

        self.main.append(DenseBlock(
            in_channels=64, growth_rate=8, layers_num=8))
        self.main.append(tch.nn.MaxPool2d(2, stride=2))

        self.main.append(DenseBlock(
            in_channels=128, growth_rate=8, layers_num=16))

        self.main.append(tch.nn.AvgPool2d(4))

        self.main = tch.nn.Sequential(*self.main)

        self.out = tch.nn.Linear(256, 10)

    def forward(self, x):

        h = self.main[:-1](x)
        o = self.main[-1](h)

        return self.out(o.view(x.size(0), -1)), h
