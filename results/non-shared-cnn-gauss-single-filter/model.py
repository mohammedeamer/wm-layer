import math

import torch as tch
import torch.nn.functional as F

from config import CONFIG

def get_gaussian_kernel(out_channels, in_channels, kernel_size=3, sigma=1):

	# Create a x, y coordinate grid of shape (kernel_size, kernel_size, 2)
	x_coord = tch.arange(kernel_size)
	x_grid = x_coord.repeat(kernel_size).view(kernel_size, kernel_size)
	y_grid = x_grid.t()
	xy_grid = tch.stack([x_grid, y_grid], dim=-1).float()

	mean = (kernel_size - 1)/2.
	variance = sigma**2.

	# Calculate the 2-dimensional gaussian kernel which is
	# the product of two gaussian distributions for two different
	# variables (in this case called x and y)
	gaussian_kernel = (1./(2.*math.pi*variance)) *\
	                  tch.exp(
	                      -tch.sum((xy_grid - mean)**2., dim=-1) /\
	                      (2*variance)
	                  )

	# Make sure sum of values in gaussian kernel equals 1.
	gaussian_kernel = gaussian_kernel / tch.sum(gaussian_kernel)

	# Reshape to 2d depthwise convolutional weight
	gaussian_kernel = gaussian_kernel.view(1, 1, kernel_size, kernel_size)
	gaussian_kernel = gaussian_kernel.repeat(out_channels, in_channels, 1, 1)

	return gaussian_kernel

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, step, out_chs, gauss_field, gauss_scale, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.gauss_field = gauss_field
		self.step = step
		self.out_chs = out_chs

		self.gauss_k = gauss_scale*get_gaussian_kernel(kernel_size=gauss_field, in_channels=in_chs, out_channels=out_chs)

		if tch.cuda.is_available():
			self.gauss_k = self.gauss_k.cuda()

		self.weights = tch.nn.Parameter(tch.FloatTensor(out_chs, in_chs, in_dim, in_dim))

		self.out_dim = int(((float(in_dim) - float(gauss_field))/float(step)) + 1.)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(self.out_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		stdv = 1./math.sqrt(self.in_chs*self.gauss_field*self.gauss_field)

		self.weights.data.uniform_(-stdv, stdv)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		# (batch, in_chs, in_dim, in_dim, out_chs)
		scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1)).view(x.size(0), -1, self.in_dim, self.in_dim)

		output = F.conv2d(scaled, self.gauss_k, stride=self.step, groups=self.out_chs)

		if self.bias is not None:
			output = output + self.bias

		return output

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))

		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
