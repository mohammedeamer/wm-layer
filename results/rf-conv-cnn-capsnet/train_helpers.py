import os
import logging as log

from logger import MainLogger as disklog

import torch as tch
import utils

def save_chkpnt(e, model, optim, best_valid_err, path):
	tch.save({'e': e, 'model': model.state_dict(), 'optim': optim.state_dict(), 'best_valid_err':best_valid_err}, os.path.join(path,"chkpnt.pt"))

def load_chkpnt(path):

	path = os.path.join(path, 'chkpnt.pt')

	if os.path.exists(path):
		return tch.load(path)

	return None

def train_epoch(model, loss_fn, perform_fn, optim, train_ds):

	loss_avg = utils.AverageMeter()
	perform_avg = utils.AverageMeter()

	batches_count = len(train_ds)

	for b, (x, y) in enumerate(train_ds):

		model.train()

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o, reconst = model(x, y)

		loss = loss_fn(o, y, reconst, x)
		perform = perform_fn(o, y)

		optim.zero_grad()
		loss.backward()
		optim.step()

		loss_avg.update(loss.item(), x.size(0))
		perform_avg.update(perform.item(), x.size(0))

		log.info("batch: {}/{} -- train_loss: {}".format(b+1, batches_count, loss_avg.avg))

	return loss_avg.avg, perform_avg.avg

def infer(model, perform_fn, valid_ds):

	with tch.no_grad():

		loss_avg = utils.AverageMeter()

		model.eval()

		batches_count = len(valid_ds)

		for b, (x, y) in enumerate(valid_ds):

			if tch.cuda.is_available():
				x = x.cuda()
				y = y.cuda()

			o, _ = model(x)

			loss = perform_fn(o, y)

			loss_avg.update(loss.item(), x.size(0))

			log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

		return loss_avg.avg

def train(model, loss_fn, perform_fn, optim_fn, train_ds, valid_ds, epochs, results_path):

	optim = optim_fn(model.parameters())

	chkpnt = load_chkpnt(results_path)

	start_e = 0

	best_valid_err = None


	if chkpnt is not None:

		model.load_state_dict(chkpnt['model'])
		optim.load_state_dict(chkpnt['optim'])

		start_e = chkpnt['e'] + 1

		best_valid_err = chkpnt['best_valid_err']

	for e in range(start_e, epochs):

		log.info("epoch: {}/{}".format(e+1, epochs))

		train_err, train_perform = train_epoch(model, loss_fn, perform_fn, optim, train_ds)
		valid_err = infer(model, perform_fn, valid_ds)

		if best_valid_err is None or valid_err < best_valid_err:

			best_valid_err = valid_err
			disklog.save_model(model)

		disklog.log_train_loss(e, train_err)
		disklog.log_train_perform(e, train_perform, valid_err)

		save_chkpnt(e, model, optim, best_valid_err, results_path)

