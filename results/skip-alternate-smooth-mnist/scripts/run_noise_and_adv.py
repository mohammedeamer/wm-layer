import argparse
import os

from adv_norm import run as run_adv
from adv_norm_mse import run as run_adv_mse
from noise_norm import run as run_noise
from noise_norm_mse import run as run_noise_mse

run_noise_fns = [run_noise, run_noise_mse]
run_adv_fns = [run_adv, run_adv_mse]

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("tag", action="store")
    parser.add_argument("model_path", action="store")

    args = parser.parse_args()

    tag = args.tag
    model_path = args.model_path

    for fn in run_noise_fns:
        fn(tag, model_path)

    for fn in run_adv_fns:

        model_1_path = os.path.join(model_path, '1')
        fn(tag, model_1_path)
