
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_chs':1, 'in_dim':28, 'gauss_field':3, 'step':1, 'out_chs':32},
			{'in_chs':32, 'in_dim':26, 'gauss_field':3, 'step':1, 'out_chs':32},
			{'in_chs':32, 'in_dim':24, 'gauss_field':3, 'step':1, 'out_chs':8}],
	'linear_layers': [{'in_features':8*22*22, 'out_features':64}, {'in_features':64, 'out_features':10}]
}


