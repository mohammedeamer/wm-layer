import sys
import os
import csv
import argparse

import torch as tch

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(dir_path, '..'))

results_path = os.path.join(dir_path, 'script-exp-results/noise-norm-mse/')


def load_model(model, trial_results_path):

    model_path = os.path.join(trial_results_path, 'model.pt')

    model.load_state_dict(tch.load(model_path, map_location=device))


def log_noise_results(results, tag):

    with open(os.path.join(results_path, '{}.csv'.format(tag)), 'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(results)


def get_model(model_path):

    from model import ResNet

    model = ResNet().to(device)

    load_model(model, model_path)

    return model


def test_model(model, ds, uniform_noise):

    from utils import AverageMeter

    with tch.no_grad():

        model.eval()

        batches_num = len(ds)

        test_avg = AverageMeter()

        for b, (x, y) in enumerate(ds):

            x = x.to(device)
            y = y.to(device)

            _, h = model(x)

            noise = tch.empty_like(x).uniform_(*uniform_noise).to(device)
            x = x + noise

            x_flat = x.reshape(x.size(0), x.size(1), -1)
            x_flat_min = x_flat.min(dim=-1, keepdim=True)[0]
            x_flat_max = x_flat.max(dim=-1, keepdim=True)[0]

            x_flat = (x_flat - x_flat_min) / (
                x_flat_max - x_flat_min)

            x = x_flat.reshape_as(x)

            _, h_ = model(x)
            perform = tch.nn.MSELoss()(h, h_)

            test_avg.update(perform.item(), x.size(0))

            print('noise: {} - batch: {}/{}'.format(uniform_noise, b + 1,
                                                    batches_num))

        return test_avg.avg


def run(tag, exp_path):

    from utils import get_ds

    os.makedirs(results_path)

    _, _, test_ds = get_ds()

    noise_ranges = [(0., 0.01), (0., 0.05),
                    (0., 0.1), (0., 0.2), (0., 0.3), (0., 0.4),
                    (0., 0.5), (0., 0.6), (0., 0.7), (0., 0.8),
                    (0., 0.9), (0., 1.)]

    err_arr = []

    trials_num = 3

    for noise in noise_ranges:

        err_sum = 0.

        for trial_idx in range(1, trials_num + 1):

            model = get_model(os.path.join(exp_path, str(trial_idx)))

            err = test_model(model, test_ds, uniform_noise=noise)

            err_sum += err

        err_arr.append(err_sum / trials_num)

    lower_arr, upper_arr = list(zip(*noise_ranges))

    results = list(zip(lower_arr, upper_arr, err_arr))

    log_noise_results(results, tag)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("tag", action="store")
    parser.add_argument("model_path", action="store")

    args = parser.parse_args()

    run(args.tag, args.model_path)
