
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_chs':1, 'in_dim':28, 'field':3, 'step':1, 'out_chs':16},
			{'in_chs':16, 'in_dim':26, 'field':3, 'step':1, 'out_chs':16},
			{'in_chs':16, 'in_dim':24, 'field':3, 'step':1, 'out_chs':1}],
	'linear_layers': [{'in_features':22*24, 'out_features':10}]
}


