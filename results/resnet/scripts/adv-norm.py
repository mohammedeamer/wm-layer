import os, importlib.util, json

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

dir_path = os.path.dirname(os.path.realpath(__file__))

results_path = os.path.join(dir_path, '../exp-results/adv-norm/')

os.makedirs(results_path)

MODELS = {
					'resnet': '../model.py',
					'resnet-fixed-kernel': '../../resnet-fixed-kernel/model.py',
					'resnet-fixed-kernel-alternate-layers': '../../resnet-fixed-kernel-alternate-layers/model.py',
				}

PARAMS = {
					'resnet': '../exp-results/resnet/1/3/model.pt',
					'resnet-fixed-kernel': '../../resnet-fixed-kernel/exp-results/resnet-fixed-kernel/1/1/model.pt',
					'resnet-fixed-kernel-alternate-layers': '../../resnet-fixed-kernel-alternate-layers/exp-results/resnet-fixed-kernel-alternate-layers/1/2/model.pt',
					}

CONFIGS = {
					'resnet': '../exp-results/resnet/1/3/model.pt',
					'resnet-fixed-kernel': '../../resnet-fixed-kernel/exp-results/resnet-fixed-kernel/1/1/model.pt',
					'resnet-fixed-kernel-alternate-layers': '../../resnet-fixed-kernel-alternate-layers/exp-results/resnet-fixed-kernel-alternate-layers/1/2/model.pt',
					}

def load_model(class_path, params_path, config_path):

	spec = importlib.util.spec_from_file_location("model", class_path)
	model = importlib.util.module_from_spec(spec)
	spec.loader.exec_module(model)

	m = model.ResNet()

	m.load_state_dict(tch.load(params_path, map_location=lambda storage, loc: storage))

	return m

def test_model(model, ds, eps=None):

	loss_fn = tch.nn.CrossEntropyLoss()

	device = tch.device("cuda" if tch.cuda.is_available() else "cpu")

	model.to(device)
	model.eval()

	points_num = len(ds)

	ds_loader = dutils.DataLoader(ds, batch_size=64)

	batches_num = len(ds_loader)

	test_err = 0.0
	confid = 0.0

	for b, (X, y) in enumerate(ds_loader):

			X = X.to(device)
			y = y.to(device)

			if eps is not None:
				X.requires_grad_()

			o = model(X)

			loss = loss_fn(o, y)

			if eps is not None:

				X = X + eps*tch.autograd.grad([loss], X)[0].sign()

				X_flat = X.view(X.size(0), X.size(1), -1)

				X_flat = (X_flat - X_flat.min(dim=-1, keepdim=True)[0])/(X_flat.max(dim=-1, keepdim=True)[0] - X_flat.min(dim=-1, keepdim=True)[0])

				X = X_flat.view_as(X)

				o = model(X)

			o_idx = o.argmax(dim=-1)
			test_err += (o_idx != y).sum().item()
			confid += tch.softmax(o, dim=-1).max(dim=-1)[0].sum(dim=0).item()

			print('{}/{}'.format(b+1, batches_num), end='\r')

	test_err = test_err/points_num
	confid = confid/points_num

	return test_err, confid

def test_model_multiple_eps(model, ds):

    errs = {}

    epsilons = [None, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.]

    for eps in epsilons:

        err, confid = test_model(model, ds, eps=eps)

        errs[str(eps)] = {'err': err, 'confid': confid}

    return errs

def run():

	mnist_path = os.path.join(dir_path, '../../../data/mnist/exp-data')
	mnist_test = dset.MNIST(root=mnist_path, train=False, download=True, transform=transforms.ToTensor())

	for k in MODELS.keys():

		class_path = os.path.join(dir_path, MODELS[k])
		params_path = os.path.join(dir_path, PARAMS[k])
		config_path = os.path.join(dir_path, CONFIGS[k])

		model = load_model(class_path=class_path, params_path=params_path, config_path=config_path)

		noise_test_errs = test_model_multiple_eps(model, mnist_test)

		with open(os.path.join(results_path, '{}.json'.format(k)), 'w') as f:
			json.dump(noise_test_errs, f)

if __name__ == '__main__':
	run()
