import math

import torch as tch
import torch.nn.functional as F

from config import CONFIG

class NonSharedConvLayer(tch.nn.Module):

	def __init__(self, in_chs, in_dim, field, step, conv_chs, skip_chs, bias=True):

		super(NonSharedConvLayer, self).__init__()

		self.in_chs = in_chs
		self.in_dim = in_dim
		self.field = field
		self.step = step
		self.conv_chs = conv_chs
		self.skip_chs = skip_chs

		self.weights = tch.nn.Parameter(tch.FloatTensor(conv_chs, in_chs, in_dim, in_dim))
		self.skip_weights = tch.nn.Parameter(tch.FloatTensor(skip_chs, in_chs, in_dim, in_dim))

		self.kernel = tch.nn.Parameter(tch.FloatTensor(conv_chs, in_chs, field, field))
		self.skip_kernel = tch.nn.Parameter(tch.FloatTensor(conv_chs, in_chs, 1, 1))

		self.out_dim = in_dim
		self.pad = math.ceil((in_dim * (step-1) - step + field)/2)

		self.bias = None

		if bias:
			self.bias = tch.nn.Parameter(tch.FloatTensor(conv_chs, self.out_dim, self.out_dim))

		self.reset_parameters()

	def reset_parameters(self):

		self.weights.data.uniform_(-1., 1.)
		self.skip_weights.data.uniform_(-1., 1.)

		stdv = 1./math.sqrt(self.in_chs*self.field*self.field)

		self.kernel.data.uniform_(-stdv, stdv)
		self.skip_kernel.data.uniform_(-1., 1.)

		if self.bias is not None:
			self.bias.data.uniform_(-stdv, stdv)

	def forward(self, x):

		# (batch, in_chs, in_dim, in_dim, out_chs)
		scaled = (self.weights.unsqueeze(0) * x.unsqueeze(1)).view(x.size(0), -1, self.in_dim, self.in_dim)

		output = F.conv2d(scaled, self.kernel, stride=self.step, padding=self.pad, groups=self.conv_chs)

		if self.bias is not None:
			output = output + self.bias

		skip_output = (self.skip_weights.unsqueeze(0) * x.unsqueeze(1)).view(x.size(0), -1, self.in_dim, self.in_dim)

		skip_output = F.conv2d(skip_output, self.skip_kernel, stride=1, groups=self.skip_chs)

		return tch.cat([output, skip_output], dim=1)

class NSCNN(tch.nn.Module):

	def __init__(self):

		super(NSCNN, self).__init__()

		conv = []

		for layer in CONFIG['conv_layers']:
			conv.append(NonSharedConvLayer(**layer))
			conv.append(tch.nn.ReLU())

		linear = []

		for l in range(len(CONFIG['linear_layers'])-1):
			linear.append(tch.nn.Linear(**CONFIG['linear_layers'][l]))
			linear.append(tch.nn.ReLU())

		linear.append(tch.nn.Linear(**CONFIG['linear_layers'][-1]))


		self.conv = tch.nn.Sequential(*conv)

		self.linear = tch.nn.Sequential(*linear)

	def forward(self, x):

		o = self.conv(x)
		return self.linear(o.view(x.size(0), -1))
