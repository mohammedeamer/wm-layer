
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			},
	'conv_layers':[{'in_chs':1, 'in_dim':28, 'field':3, 'step':1, 'conv_chs':16, 'skip_chs': 16},
			{'in_chs':32, 'in_dim':28, 'field':3, 'step':1, 'conv_chs':16, 'skip_chs': 16},
			{'in_chs':32, 'in_dim':28, 'field':3, 'step':1, 'conv_chs':4, 'skip_chs': 4}],
	'linear_layers': [{'in_features':8*28*28, 'out_features':64}, {'in_features':64, 'out_features':10}]
}


